package com.nitaameliawijaya.lesson_bukalapak_duplicate_v11;

public class Barang {
    public Integer idBarang;
    public String namaBarang;
    public Integer hargaBarang;
    public String detailBarang;
    public Integer isChoosen = 0;
    public Integer imageURL;

    public Barang(Integer pIdBarang,
                  String pNamaBarang,
                  String pDetailBarang,
                  Integer pImageURL,
                  Integer pHargaBarang,
                  Integer pIsChoosen){
        this.idBarang = pIdBarang;
        this.namaBarang = pNamaBarang;
        this.detailBarang = pDetailBarang;
        this.imageURL = pImageURL;
        this.hargaBarang = pHargaBarang;
        this.isChoosen = pIsChoosen;
    }

    public Integer getIdBarang() {  return idBarang;   }

    public String getNamaBarang() {
        return namaBarang;
    }

    public String getDetailBarang() {
        return detailBarang;
    }

    public Integer getImageURL() {
        return imageURL;
    }

    public Integer getHargaBarang() {
        return hargaBarang;
    }

    public Integer getIsChoosen() {
        return isChoosen;
    }
}