package com.nitaameliawijaya.lesson_bukalapak_duplicate_v11;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MyDBSQLite.db";
    public static final String TABLE_NAME = "listbarang";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAMA_BARANG = "nama_barang";
    public static final String COLUMN_HARGA_BARANG = "harga_barang";
    public static final String COLUMN_DESK_BARANG = "deskripsi_barang";
    public static final String COLUMN_STATUS_BARANG = "status_barang";
    public static final String COLUMN_IMAGE_BARANG = "image_barang";
    private static final String SQL_CREATE_ENTRIES =
            "create table " + TABLE_NAME + " ("+ COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_NAMA_BARANG + " TEXT," +
                    COLUMN_HARGA_BARANG + " TEXT,"+
                    COLUMN_DESK_BARANG + " TEXT,"+
                    COLUMN_STATUS_BARANG + " TEXT,"+
                    COLUMN_IMAGE_BARANG + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public boolean insertData(String namaBarang, Integer hargaBarang, String deskBarang, Integer isChoosen, Integer imageBarang) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAMA_BARANG, namaBarang);
        contentValues.put(COLUMN_HARGA_BARANG, hargaBarang);
        contentValues.put(COLUMN_DESK_BARANG, deskBarang);
        contentValues.put(COLUMN_STATUS_BARANG, isChoosen);
        contentValues.put(COLUMN_IMAGE_BARANG, imageBarang);
        long result = db.insert(TABLE_NAME, null, contentValues);
        Log.d("result", (result + ""));
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }

    }

    public boolean updateData(Integer idBarang, String namaBarang, Integer hargaBarang, String deskBarang, Integer isChoosen, Integer imageBarang){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAMA_BARANG, namaBarang);
        contentValues.put(COLUMN_HARGA_BARANG, hargaBarang);
        contentValues.put(COLUMN_DESK_BARANG, deskBarang);
        contentValues.put(COLUMN_STATUS_BARANG, isChoosen);
        contentValues.put(COLUMN_IMAGE_BARANG, imageBarang);
        String[] tempId = {Integer.toString(idBarang)};
        long result = db.update(TABLE_NAME, contentValues,COLUMN_ID+"= ?", tempId);
        Log.d("result update", (result + ""));
        db.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getTroli(){
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                COLUMN_NAMA_BARANG,
                COLUMN_HARGA_BARANG,
                COLUMN_DESK_BARANG,
                COLUMN_STATUS_BARANG,
                COLUMN_IMAGE_BARANG
        };

        String selection = COLUMN_STATUS_BARANG+" = ?";
        String[] selectionArgs = { Integer.toString(1) };
        Cursor cursor = db.query(
                TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
        return cursor;
    }

    public Cursor getAllData(){
        SQLiteDatabase db = this.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                COLUMN_NAMA_BARANG,
                COLUMN_HARGA_BARANG,
                COLUMN_DESK_BARANG,
                COLUMN_STATUS_BARANG,
                COLUMN_IMAGE_BARANG
        };
        Cursor cursor = db.query(
                TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        return cursor;
    }
}
