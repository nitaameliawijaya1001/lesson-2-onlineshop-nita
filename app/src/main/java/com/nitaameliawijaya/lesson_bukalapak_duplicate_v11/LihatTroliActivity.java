package com.nitaameliawijaya.lesson_bukalapak_duplicate_v11;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class LihatTroliActivity extends AppCompatActivity {
    DBHelper mydb;
    TroliAdapter adapter;
    ArrayList<Barang> arrayOfBarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_troli);
        mydb = new DBHelper(this);
        arrayOfBarang = new ArrayList<Barang>();

        // Create the adapter to convert the array to views
        this.adapter = new TroliAdapter(this, arrayOfBarang);

        ListView listView = findViewById(R.id.list_view_troli);
        listView.setAdapter(adapter);

        showTroli();
    }

    public void showTroli(){
        Cursor cursor = mydb.getTroli();

        while(cursor.moveToNext()){
            Integer tempId = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_ID));
            String tempNama = cursor.getString(cursor.getColumnIndexOrThrow(mydb.COLUMN_NAMA_BARANG));
            Integer tempHarga = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_HARGA_BARANG));
            String tempDesk = cursor.getString(cursor.getColumnIndexOrThrow(mydb.COLUMN_DESK_BARANG));
            Integer tempChoosen = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_STATUS_BARANG));
            Integer tempImage = cursor.getInt(cursor.getColumnIndexOrThrow(mydb.COLUMN_IMAGE_BARANG));

            adapter.add(new Barang(tempId, tempNama, tempDesk, tempImage, tempHarga, tempChoosen));
        }

        cursor.close();
        mydb.close();
    }
}
