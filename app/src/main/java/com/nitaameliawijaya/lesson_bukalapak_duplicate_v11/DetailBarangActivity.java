package com.nitaameliawijaya.lesson_bukalapak_duplicate_v11;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailBarangActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang);

        initGetIntent();
    }

    public void initGetIntent(){
        TextView textViewDetailNamaBarang = findViewById(R.id.text_view_nama_barang_detail);
        TextView textViewDetailDeskripsi = findViewById(R.id.text_view_deskripsi);
        TextView textViewDetailHarga = findViewById(R.id.text_view_harga_barang_detail);
        ImageView imageViewDetailBarang = findViewById(R.id.image_view_detail);

        String parseNamaBarang = "";
        Integer parseHargaBarang;
        String parseDeskBarang = "";
        Integer parseStatusBarang;
        Integer parseImage;

        boolean checkNamaBarang = getIntent().hasExtra("parse_nama_barang");
        boolean checkHargaBarang = getIntent().hasExtra("parse_harga_barang");
        boolean checkDeskBarang = getIntent().hasExtra("parse_desk_barang");
        boolean checkStatusBarang = getIntent().hasExtra("parse_status_barang");
        boolean checkImage = getIntent().hasExtra("parse_image_barang");

        if(checkNamaBarang && checkHargaBarang && checkDeskBarang && checkStatusBarang && checkImage){
            parseNamaBarang = getIntent().getExtras().getString("parse_nama_barang");
            parseHargaBarang = getIntent().getExtras().getInt("parse_harga_barang");
            parseDeskBarang = getIntent().getExtras().getString("parse_desk_barang");
            parseStatusBarang = getIntent().getExtras().getInt("parse_status_barang");
            parseImage = getIntent().getExtras().getInt("parse_image_barang");

            textViewDetailNamaBarang.setText(parseNamaBarang);
            textViewDetailHarga.setText(parseHargaBarang+"");
            textViewDetailDeskripsi.setText(parseDeskBarang);
            imageViewDetailBarang.setImageResource(parseImage);
        }else{
            textViewDetailNamaBarang.setText("Key is False!");
            textViewDetailHarga.setText("Key is False!");
            textViewDetailDeskripsi.setText("Key is False!");
            imageViewDetailBarang.setImageResource(R.drawable.image_11);
        }

    }
}
