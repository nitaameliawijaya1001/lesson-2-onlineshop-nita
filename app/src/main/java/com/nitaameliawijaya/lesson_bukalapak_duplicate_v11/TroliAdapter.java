package com.nitaameliawijaya.lesson_bukalapak_duplicate_v11;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class TroliAdapter extends ArrayAdapter<Barang> {
    int i;

    public class ViewHolder{
        ImageView imageView;
        TextView textViewNama;
        TextView textViewHarga;
    }

    public TroliAdapter(Context mContext, ArrayList<Barang> mBarang){
        super(mContext, 0, mBarang);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Barang barang = getItem(position);
        final ViewHolder holder;


        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.detail_troli,parent,false);
            holder = new ViewHolder();

            holder.imageView = convertView.findViewById(R.id.image_view_barang_troli);
            holder.textViewNama = convertView.findViewById(R.id.text_view_nama_barang_troli);
            holder.textViewHarga = convertView.findViewById(R.id.text_view_harga_barang_troli);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        TextView textViewNamaBarang = convertView.findViewById(R.id.text_view_nama_barang_troli);
        TextView textViewHargaBarang = convertView.findViewById(R.id.text_view_harga_barang_troli);
        ImageView imageViewBarang = convertView.findViewById(R.id.image_view_barang_troli);

        textViewNamaBarang.setText(barang.namaBarang);
        textViewHargaBarang.setText(barang.hargaBarang+"");
        imageViewBarang.setImageResource(barang.imageURL);

        return convertView;
    }
}
