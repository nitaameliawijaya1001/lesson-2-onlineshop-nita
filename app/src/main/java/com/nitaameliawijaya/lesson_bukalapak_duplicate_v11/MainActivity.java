package com.nitaameliawijaya.lesson_bukalapak_duplicate_v11;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public BarangAdapter adapter;

    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mydb = new DBHelper(getApplicationContext());
        initData();
    }

    public void initData(){
        int[] arrayImage = {
                R.drawable.image_1,
                R.drawable.image_2,
                R.drawable.image_3,
                R.drawable.image_4,
                R.drawable.image_5,
                R.drawable.image_6,
                R.drawable.image_7,
                R.drawable.image_8,
                R.drawable.image_9,
                R.drawable.image_10};
        boolean isInsert;
        int i;
        for(i=0; i<10; i++){
            isInsert = mydb.insertData("Sepatu "+ i, 150000+(i*5000)/(i+10), "Deskripsi Bla-bla-bla-bla-bla", 0, arrayImage[i]);
            if(isInsert){
                Log.d("cekInsert", "Insert ke : " + i + "Berhasil !!!" );
            }else{
                Log.d("cekInsert", "Insert ke : " + i + "Gak Berhasil !!!" );
            }
        }
    }

    public void goToLihatBarangScreen(View view){
        Intent intentLihatBarang = new Intent(getApplicationContext(), LihatBarangActivity.class);
        startActivity(intentLihatBarang);
    }

    public void goToLihatTroliScreen(View view){
        Intent intentLihatTroli = new Intent(getApplicationContext(), LihatTroliActivity.class);
        startActivity(intentLihatTroli);
    }

    public void clearItems(){
        this.adapter.clear();
    }

    protected void onDestroy(){
        mydb.close();
        super.onDestroy();
    }
}
