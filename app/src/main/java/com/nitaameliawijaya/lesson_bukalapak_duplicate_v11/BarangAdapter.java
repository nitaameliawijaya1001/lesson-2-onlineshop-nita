package com.nitaameliawijaya.lesson_bukalapak_duplicate_v11;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BarangAdapter extends ArrayAdapter<Barang> {
    DBHelper mydb;

    public class ViewHolder{
        ImageView imageView;
        TextView textViewNama;
        TextView textViewHarga;
        Button btnPilih;
        Button btnView;
    }

    public BarangAdapter(Context mContext, ArrayList<Barang> mBarang){
        super(mContext, 0, mBarang);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Barang barang = getItem(position);
        final ViewHolder holder;


        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.detail_barang,parent,false);
            holder = new ViewHolder();

            holder.imageView = convertView.findViewById(R.id.image_view_barang);
            holder.textViewNama = convertView.findViewById(R.id.text_view_nama_barang);
            holder.textViewHarga = convertView.findViewById(R.id.text_view_harga_barang);
            holder.btnPilih = convertView.findViewById(R.id.button_pilih_barang);
            holder.btnView = convertView.findViewById(R.id.button_view_detail);

            if(barang.isChoosen == 1){
                Log.d("cek button isChoosen : ", " "+barang.isChoosen);
                holder.btnPilih.setText("BATAL");
            }else{
                holder.btnPilih.setText("PILIH BARANG");
            }
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
            Log.d("cek barang", "isChoosen : "+barang.isChoosen);
            if(barang.isChoosen == 1){
                holder.btnPilih.setText("BATAL");
            }else{
                holder.btnPilih.setText("PILIH BARANG");
            }
        }

        Button buttonViewDetail = convertView.findViewById(R.id.button_view_detail);
        final Button buttonPilihBarang = convertView.findViewById(R.id.button_pilih_barang);

        buttonViewDetail.setTag(position);
        buttonPilihBarang.setTag(position);

        buttonViewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Barang barang = getItem(position);
                Intent intenViewDetail = new Intent(v.getContext(), DetailBarangActivity.class);
                intenViewDetail.putExtra("parse_nama_barang", barang.namaBarang);
                intenViewDetail.putExtra("parse_harga_barang", barang.hargaBarang);
                intenViewDetail.putExtra("parse_desk_barang", barang.detailBarang);
                intenViewDetail.putExtra("parse_status_barang", barang.isChoosen);
                intenViewDetail.putExtra("parse_image_barang", barang.imageURL);
                v.getContext().startActivity(intenViewDetail);
            }
        });

        buttonPilihBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Barang barang = getItem(position);
                if(barang.isChoosen == 1){
                    barang.isChoosen = 0;
                    buttonPilihBarang.setText("PILIH BARANG");
                }else{
                    barang.isChoosen = 1;
                    buttonPilihBarang.setText("BATAL");
                }
                mydb = new DBHelper(v.getContext());
                if(mydb.updateData(barang.idBarang,barang.namaBarang,barang.hargaBarang,barang.detailBarang,barang.isChoosen,barang.imageURL)){
                    Log.d("cek Update ke "+position+" : ", "BERHASIL!!");
                }else{
                    Log.d("cek Update ke "+position+" : ", "GAK BERHASIL!!");
                }
                mydb.close();
            }
        });



        TextView textViewNamaBarang = convertView.findViewById(R.id.text_view_nama_barang);
        TextView textViewHargaBarang = convertView.findViewById(R.id.text_view_harga_barang);
        ImageView imageViewBarang = convertView.findViewById(R.id.image_view_barang);

        textViewNamaBarang.setText(barang.namaBarang);
        textViewHargaBarang.setText(barang.hargaBarang+"");
        imageViewBarang.setImageResource(barang.imageURL);

        return convertView;
    }
}
